#pragma once

template<class T>
class Kd_tree {
public:
	/// <summary>
	/// Represents a single node of the tree.
	/// </summary>
	struct Node {

		/// <summary>
		/// Default constructor.
		/// </summary>
		Node() = default;

		/// <summary>
		/// Construct a node. Children are optional.
		/// </summary>
		Node(std::vector<T> cds, Node* l = nullptr, Node* r = nullptr) : coords(cds), l_child(l), r_child(r) {};

		/// <summary>
		/// Returns true if node doesn't have any children.
		/// </summary>
		inline bool is_leaf() const { return (l_child || r_child) ? false : true; }

		/// <summary>
		/// Left child of the node.
		/// </summary>
		Node *l_child = nullptr;

		/// <summary>
		/// Right child of the node.
		/// </summary>
		Node *r_child = nullptr;

		/// <summary>
		/// Coordinates of the node.
		/// </summary>
		std::vector<T> coords;
	};

	/// <summary>
	/// Creates a tree by inserting points in provided order (faster).
	/// </summary>
	/// <param name="points">List of points from which the tree will be created.</param>
	void build(std::vector<std::vector<T>>& points);

	/// <summary>
	/// Creates a balanced k-d tree (slower).
	/// </summary>
	/// <param name="points"></param>
	void build_balanced(std::vector<std::vector<T>>& points);

	/// <summary>
	/// Inserts a node into the tree.
	/// </summary>
	void insert(Node* node);

	/// <summary>
	/// Inserts a point into the tree.
	/// </summary>
	void insert(std::vector<T>& point);

	/// <summary>
	/// Returns a node that has same coords as the provided node if it exists or else returns a nullptr.
	/// </summary>
	/// <param name="root">Root of the (sub)tree that will be searched.</param>
	Node* get(Node* node, Node* root) const;

	/// <summary>
	/// Returns a node that has same coords as the provided point if it exists or else returns a nullptr.
	/// </summary>
	/// <param name="root">Root node from which the search will start.</param>
	Node* get(std::vector<T>& point, Node* root) const;

	/// <summary>
	/// Returns the nearest node to the provided point.
	/// </summary>
	/// <param name="root">Root node of the (sub)tree that will be searched.</param>
	/// <param name="point">Requested point.</param>
	/// <returns>Nearest node to the point.</returns>
	Node* nearest_neighbour(const Node* root, const std::vector<T>& point) const;

	/// <summary>
	/// Returns true if the (sub)tree contains the node.
	/// </summary>
	/// <param name="node">Node in question.</param>
	/// <param name="root">Root node.</param>
	bool contains(Node* node, Node* root) const;

	/// <summary>
	/// Returns true if the (sub)tree contains the point.
	/// </summary>
	/// <param name="point">Point in question.</param>
	/// <param name="root">Root node.</param>
	bool contains(std::vector<T>& point, Node* root) const;

	/// <summary>
	/// Size of the tree.
	/// </summary>
	int size() const;

	/// <summary>
	/// Size of the (sub)tree.
	/// </summary>
	/// <param name="node">Root node.</param>
	int size(Node* node) const;

	/// <summary>
	/// Returns second power of the distance between points in kD space. 
	/// Calculate root of this value to get the actual distance.
	/// </summary>
	/// <param name="lhs">First node.</param>
	/// <param name="rhs">Second node.</param>
	/// <param name="ax">First n dimensions to be taken into consideration.</param>
	double distance_between_nodes(const Node* lhs, const Node* rhs, int ax) const;

	/// <summary>
	/// Depth of the tree. 
	/// </summary>
	int max_depth() const;

	/// <summary>
	/// Depth of the (sub)tree.
	/// </summary>
	/// <param name="node">Root node.</param>
	int max_depth(Node* node) const;

	/// <summary>
	/// Print the tree inorder and inline.
	/// </summary>
	/// <param name="node">Starting node.</param>
	void print_inline(const Node* node) const;

	/// <summary>
	/// Visual representation of the tree structure.
	/// </summary>
	/// <param name="node">Starting node.</param>
	/// <param name="depth">[Optional] Starting depth.</param>
	void print_structured(const Node* node, int depth = -1) const;

	/// <summary>
	/// Dimension of the tree.
	/// </summary>
	unsigned int K;

	/// <summary>
	/// Root node.
	/// </summary>
	Node* root = nullptr;

private:
	typedef typename std::vector<std::vector<T>>::iterator vv_iterator;
	void nearest_neighbour_r(const Node* node, const std::vector<T>& point, Node& closest, long double& minDist, int depth = 0) const;
	void traverse_and_insert(Node* node, Node* cmp, int n = 0);
	int max_depth_r(Node* node) const;
	Node* traverse_and_find(Node* node, Node* target) const;
	Node* construct(std::vector<std::vector<T>>& points, vv_iterator begin, vv_iterator end, int ax);
};