#include <vector>
#include <string>
#include <algorithm>
#include <stdlib.h>
#include <iostream>
#include <chrono>

#include "kd_tree.h"

template<class T>
void Kd_tree<T>::build(std::vector<std::vector<T>>& points)
{
	if (points.size() > 0)
	{
		K = points[0].size();
		if (K > 0)
		{
			if (!root) root = new Node(points[0]);
			for (auto it = std::next(points.begin()); it != points.end(); it++)
			{
				traverse_and_insert(new Node(*it), root, 0);
			}
		}
	}
}

template<class T>
void Kd_tree<T>::build_balanced(std::vector<std::vector<T>>& points)
{
	if (points.size() > 0)
	{
		K = points[0].size();
		if (K > 0)
		{
			root = construct(points, points.begin(), points.end(), 0);
		}
	}
}

template<class T>
typename Kd_tree<T>::Node* Kd_tree<T>::construct(std::vector<std::vector<T>>& points, vv_iterator begin, vv_iterator end, int ax) 
{
	int dist = std::distance(begin, end);

	if (dist > 1)
	{
		ax = ax % K;
		vv_iterator middle = std::next(begin, dist / 2);
		std::nth_element(begin, middle, end, [ax](const std::vector<T>& a, const std::vector<T>& b) { return a[ax] < b[ax]; });
		Node* n = new Node(*middle);
		ax++;
		return new Node(*middle, construct(points, begin, middle, ax), construct(points, std::next(middle), end, ax));
	}
	else if (dist == 1)
	{
		 return new Node(*begin);
	}
	else 
	{
		return nullptr;
	}

}

template<class T>
void Kd_tree<T>::insert(typename Kd_tree<T>::Node* node) {
	traverse_and_insert(node, root, 0);
}

template<class T>
void Kd_tree<T>::insert(std::vector<T>& point) {
	insert(new Node(point));
}

template<class T>
bool Kd_tree<T>::contains(typename Kd_tree<T>::Node* target, typename Kd_tree<T>::Node* node) const 
{
	if (traverse_and_find(target, node))
		return true;
	else
		return false;
}

template<class T>
bool Kd_tree<T>::contains(std::vector<T>& point, typename Kd_tree<T>::Node* node) const 
{
	if (traverse_and_find(new Node(point), node))
		return true;
	else
		return false;
}

template<class T>
typename Kd_tree<T>::Node* Kd_tree<T>::get(std::vector<T>& point, typename Kd_tree<T>::Node* root) const 
{
	return traverse_and_find(new Node(point), root);
}

template<class T>
typename Kd_tree<T>::Node* Kd_tree<T>::get(typename Kd_tree<T>::Node* node, typename Kd_tree<T>::Node* root) const 
{
	return traverse_and_find(node, root);
}

template<class T>
typename Kd_tree<T>::Node* Kd_tree<T>::nearest_neighbour(const typename Kd_tree<T>::Node* root, const std::vector<T>& point) const 
{
	Node* nn = new Node();
	long double min_dist = std::numeric_limits<long double>::max();
	nearest_neighbour_r(root, point, *nn, min_dist, 0);
	return nn;
}

template<class T>
void Kd_tree<T>::nearest_neighbour_r(const typename Kd_tree<T>::Node* node, 
	const std::vector<T>& point, typename Kd_tree<T>::Node& closest, long double& min_dist, int depth) const 
{
	if (node)
	{
		Node* cmpNode = new Node(point);
		const double dist = distance_between_nodes(node, cmpNode, K);

		if (dist < min_dist)
		{
			min_dist = dist;
			closest = *node;
		}
		if (min_dist == 0) return;

		int ax = ((depth) % K);
		if (cmpNode->coords[ax] < node->coords[ax])
		{
			nearest_neighbour_r(node->l_child, point, closest, min_dist, depth + 1);
			if (cmpNode->coords[ax] + min_dist >= node->coords[ax])
			{
				nearest_neighbour_r(node->r_child, point, closest, min_dist, depth + 1);
			}

		}
		else 
		{
			nearest_neighbour_r(node->r_child, point, closest, min_dist, depth + 1);
			if (cmpNode->coords[ax] - min_dist <= node->coords[ax])
			{
				nearest_neighbour_r(node->l_child, point, closest, min_dist, depth + 1);
			}
		}
	}
}

template<class T>
typename Kd_tree<T>::Node* Kd_tree<T>::traverse_and_find(typename Kd_tree<T>::Node* target, typename Kd_tree<T>::Node* node) const 
{

	if (!node) 
		return nullptr;

	if (node->coords == target->coords) 
		return node;

	auto l = traverse_and_find(target, node->l_child);
	if (l) return l;

	auto r = traverse_and_find(target, node->r_child);
	if (r) return r;

	return nullptr;
}

template<class T>
void Kd_tree<T>::traverse_and_insert(typename Kd_tree<T>::Node* node, typename Kd_tree<T>::Node* cmp, int n) 
{
	int ax = ((n++) % K);
	if (node->coords[ax] < cmp->coords[ax]) 
	{
		if (cmp->l_child) traverse_and_insert(node, cmp->l_child, n);
		else cmp->l_child = node;
	}
	else 
	{
		if (cmp->r_child) traverse_and_insert(node, cmp->r_child, n);
		else cmp->r_child = node;
	}
}

template<class T>
int Kd_tree<T>::max_depth() const 
{
	return max_depth_r(root) - 1;
}

template<class T>
int Kd_tree<T>::max_depth(typename Kd_tree<T>::Node* node) const 
{
	return max_depth_r(node) - 1;
}

template<class T>
int Kd_tree<T>::max_depth_r(typename Kd_tree<T>::Node* node) const 
{
	if (!node) return 0;
	int l_depth = max_depth_r(node->l_child);
	int r_depth = max_depth_r(node->r_child);
	return ++(l_depth > r_depth ? l_depth : r_depth);
}

template<class T>
int Kd_tree<T>::size() const 
{
	return size(root);
}

template<class T>
int Kd_tree<T>::size(typename Kd_tree<T>::Node* node) const 
{
	if (node)
		return size(node->l_child) + size(node->r_child) + 1;
	return 0;
}

template <class T>
double Kd_tree<T>::distance_between_nodes(const typename Kd_tree<T>::Node* lhs, const typename Kd_tree<T>::Node* rhs, int ax) const 
{
	double tmp, dist = 0;
	while (ax--) 
	{
		tmp = lhs->coords[ax] - rhs->coords[ax];
		dist += tmp * tmp;
	}
	return dist;
}

template<class T>
void Kd_tree<T>::print_inline(const typename Kd_tree<T>::Node* node) const 
{
	if (node) 
	{
		print_inline(node->l_child);
		std::cout << "< ";
		for (auto &a : node->coords) std::cout << a << " ";
		std::cout << ">";
		print_inline(node->r_child);
	}
}

template<class T>
void Kd_tree<T>::print_structured(const typename Kd_tree<T>::Node* node, int depth) const 
{
	if (node) 
	{
		depth++;
		int indent = depth * 8;
		int half_indent = 4;
		if (node->r_child) 
		{
			print_structured(node->r_child, depth);
			std::cout << "\r" << std::string(indent + half_indent + 2, ' ') << "/" << std::endl;
			std::cout << "\r" << std::string(indent + half_indent, ' ') << "/" << std::endl;
		}

		std::cout << "\r" << std::string(indent, ' ') << "< ";
		for (auto &a : node->coords) std::cout << a << " ";
		std::cout << ">" << std::endl;

		if (node->l_child) 
		{
			std::cout << "\r" << std::string(indent + half_indent, ' ') << "\\" << std::endl;
			std::cout << "\r" << std::string(indent + half_indent + 2, ' ') << "\\" << std::endl;
			print_structured(node->l_child, depth);
		}
	}
}

int main() {
	/*
	int point_count = 10000;
	std::vector<std::vector<int>> points;
	for (int i = 0; i < point_count; i++) {
		points.emplace_back(std::vector<int>{rand() % 100, 100 - (rand() % 100)});
	}

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;

	Kd_tree<int> fast_tree;
	t1 = std::chrono::high_resolution_clock::now();
	fast_tree.build_fast(points);
	t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	std::cout << std::endl << "Fast build time: " << duration << " us" << std::endl;
	*/

	std::vector<std::vector<int>> points{ { 8,1 }, { 4,7 }, {7,2}, {5,4}, {9,6}, {2,3} };
	Kd_tree<int> tree;
	tree.build_balanced(points);

	tree.insert(std::vector<int>{5, 5});
	Kd_tree<int>::Node* nd = new Kd_tree<int>::Node(std::vector<int>{6, 6});
	tree.insert(nd);
	if (tree.contains(nd, tree.root)) { std::cout << "Contains" << std::endl; }
	if (tree.contains(std::vector<int>{5, 5}, tree.root)) { std::cout << "Contains" << std::endl; }
	std::cout << tree.get(std::vector<int>{5, 5}, tree.root)->coords[0] << std::endl;
	std::cout << tree.get(nd, tree.root)->coords[0] << std::endl;

	tree.print_inline(tree.root);
	std::cout << std::endl;
	tree.print_structured(tree.root, -1);
	std::cout << "Size from node: " << tree.size(tree.get(std::vector<int>{5, 5}, tree.root)) << std::endl;
	std::cout << "Depth from node: " << tree.max_depth(tree.get(std::vector<int>{5, 5}, tree.root)) << std::endl;
	std::cout << tree.size() << std::endl;

	auto nn = tree.nearest_neighbour(tree.root, std::vector<int>{12, 5});
	if (nn) {
		std::cout << "Closest node: " << nn->coords[0] << ", " << nn->coords[1] << std::endl;
	}

	return 0;
}